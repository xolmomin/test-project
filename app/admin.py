from django.contrib import admin
from django.contrib.auth.models import Group

from app import models


@admin.register(models.Post)
class CategoryAdmin(admin.ModelAdmin):
    fields = ('title', 'slug', 'body')
    prepopulated_fields = {'slug': ('title',)}

    def save_model(self, request, obj, form, change):
        obj.owner = request.user
        super().save_model(request, obj, form, change)


admin.site.unregister(Group)
