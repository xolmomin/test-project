from django.urls import path
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView
from rest_framework_simplejwt import views as jwt_views

from app import views

urlpatterns = [
    path('analitics/', views.AnaliticsListAPIView.as_view(), name='analitics'),
    path('post/', views.PostListCreateAPIView.as_view(), name='post'),
    path('post-like/<int:pk>', views.PostLikeAPIView.as_view(), name='post_like'),  # pk - post id
]

# Auth
urlpatterns += [
    path('register/', views.RegisterApi.as_view(), name='register'),
    path('login/', jwt_views.TokenObtainPairView.as_view(), name='login'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path('schema/', SpectacularAPIView.as_view(), name='schema'),
]
