from django.contrib.auth.models import User
from django.utils.deprecation import MiddlewareMixin
from django.utils.timezone import now

from .models import ActivityLog


class ActivityLogMiddleware(MiddlewareMixin):

    def process_response(self, request, response):
        if response.status_code == 200 and request.path == '/api/login/':
            username = request.POST.get('username')
            if username:
                user = User.objects.get(username=username)
                self._write_log(request, user)

        if request.user.is_authenticated:
            user = request.user
            self._write_log(request, user)
        return response

    def _write_log(self, request, user):
        activity_log, created = ActivityLog.objects.get_or_create(user=user)
        activity_log.request_method = request.method
        if request.path == '/api/login/':
            activity_log.last_login = now()
        activity_log.request_url = request.build_absolute_uri()
        activity_log.save()
