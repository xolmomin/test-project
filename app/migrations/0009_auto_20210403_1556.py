# Generated by Django 3.1.7 on 2021-04-03 15:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_auto_20210403_1525'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activitylog',
            name='request_method',
            field=models.CharField(max_length=50, verbose_name='method of last request'),
        ),
    ]
